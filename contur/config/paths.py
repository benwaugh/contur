import os
import os.path

def root_path():
    """
    Get the root path of the contur installation
    """
    croot = os.environ.get("CONTUR_ROOT", os.environ.get("CONTURMODULEDIR"))
    if croot is None:
        croot = get_root_path_from_os()
    return croot

def get_root_path_from_os():
    """
    Get the root path of the contur installation using the fact that this
    file is in the subdirectory CONTUR_ROOT/contur/config/
    """
    parent1 = os.path.dirname(__file__)
    parent2 = os.path.dirname(parent1)
    parent3 = os.path.dirname(parent2)
    return parent3

def path(*relpaths):
    """
    Get the complete path to a sub-path in the Contur installation
    """
    return os.path.join(root_path(), *relpaths)
