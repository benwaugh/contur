# -*- python -*-

"""
Miscellaneous helper functions that may be used by more than one contur submodule

"""

import os
import numpy as np
import contur
from builtins import input
import contur.config.config as cfg
import contur.config.version
import contur.data.static_db as cdb
import contur.factories.likelihood as lh


import scipy.stats as spstat

## Import the tqdm progress-bar if possible, otherwise fall back to a safe do-nothing option
try:
    from tqdm import tqdm as progress_bar
except ImportError:
    def progress_bar(iterable, **kwargs):
        return iterable

def splitPath(path):
    """
    Take a yoda histogram path and return the analysis name and the histogram name
    :arg path: the full path of a yoda analysis object
    :type: String

    """
    from rivet.aopaths import AOPath

    aop = AOPath(path)
    parts = AOPath.dirnameparts(aop)
    analysis = parts[len(parts)-1]
    h_name = AOPath.basename(aop)
    return analysis, h_name


def mkoutdir(outdir):
    """
    Function to make an output directory if it does not already exist.
    Also tests if an existing directory is write-accessible.
    """
    if not os.path.exists(outdir):
        try:
            os.makedirs(outdir)
        except:
            msg = "Can't make output directory '%s'" % outdir
            raise Exception(msg)
    if not os.access(outdir, os.W_OK):
        msg = "Can't write to output directory '%s'" % outdir
        raise Exception(msg)


def write_banner():
    """
    Write a text banner giving the version and pointing to the documenation
    """
    msgs = ["Contur version {}".format(contur.config.version.version),
            "See https://hepcedar.gitlab.io/contur-webpage/"]
    for m in msgs:
        try:
            cfg.contur_log.info(m)
        except:
            print(m)


def write_summary_file(message, conturDepot):
    """
    Write a brief text summary of a conturDepot, describing the run conditions and results.
    If cfg.gridMode is False, will also write info about the
    most sensitive histograms in each pool, for parsing by contur-mkhtml

    :param message: text (string) message containing the run conditions for this depot

    :param conturDepot: the processed depot of results

    the name of the directory to write the file into will be determined by cfg.output_dir

    """

    mkoutdir(cfg.output_dir)
    sumfn = open(os.path.join(cfg.output_dir,cfg.summary_file), 'w')

    if cfg.gridMode:
        sumfn.write(message)
    else:

        # summary function will just read the first entry in the depot inbox
        result = ""
        for stat_type in cfg.stat_types:
            try:
                result += "\nCombined {} exclusion for these plots is {:15.13f} % ".format(stat_type, conturDepot.inbox[0].yoda_factory.get_full_likelihood(stat_type).getCLs() * 100.0)
            except:
                result += "\nCould not evaluate {} exclusion for these data.".format(stat_type)

        sumfn.write(message + "\n" + result + "\n")
        sumfn.write("\npools")

        yfactory = conturDepot.inbox[0].yoda_factory
        for pool in cdb.get_pools():
            pool_summary = "\nPool:{}\n".format(pool)

            
            got_it = []
            for lh in yfactory.likelihood_blocks:            
                if lh.pools == pool:
                    for stat_type in cfg.stat_types:
                        if yfactory.get_sorted_likelihood_blocks(stat_type) is not None and lh in yfactory.get_sorted_likelihood_blocks(stat_type):
                            pool_summary+="Exclusion with {}={:.8f}\n".format(stat_type,lh.getCLs(stat_type))
                            pool_summary+="{}\n".format(lh.tags)
                            got_it.append(stat_type)
                            
            if len(got_it)>0:
                for stat_type in cfg.stat_types:
                    if not stat_type in got_it:
                        pool_summary+="No exclusion evaluated for {}\n".format(stat_type,lh.getCLs(stat_type))
                sumfn.write(pool_summary)
                                
        cfg.contur_log.info(result)

        sumfn.close()



def write_yoda_dat(observable, nostack=False, smtest=False):
    """
    Write a YODA  .dat file (plot formatting) for the histograms in the output directory, for later display.

    :param nostack: flag saying whether to plot data along or stack it on the background (default) 
    :type boolean:

    :param observable: dressed YODA ao
    :type observable: :class:`contur.factories.Observable`

    the output directory is determined by cfg.plot_dir

    """

    import yoda, rivet
    
    ## Check if we have reference data for this observable. If not, then abort.
    if not observable.ref:
        cfg.contur_log.warning("Not writing dat file for {}. No REF data.".format(observable.signal.path()))
        return
    
    # List for the analysis objects which will be in the dat file
    anaobjects = []
    # List for the names of  analysis objects which will be drawn
    drawonly = []


    
    # get the data and signal+data plots
    refdata = observable.refplot
    if nostack:
        # in this case we are just plotting signal, without stacking it on any background
        sigback_databg  = observable.sigplot
    else:
        sigback_databg = observable.stack_databg

    # set annotations for the reference data
    refdata.setAnnotation('ErrorBars', '1')
    refdata.setAnnotation('PolyMarkers', '*')
    refdata.setAnnotation('ConnectBins', '0')
    experiment = refdata.path().split("_")[0][5:]
    refdata.setAnnotation('Title', '{} Data'.format(experiment))
    drawonly.append(refdata.path())
    anaobjects.append(refdata)

        
    # set annotations for the data-as-background signal plot
    if sigback_databg is not None and not smtest:

        # Calculate the databg CLs for this individual plot 
        contur.factories.likelihood.likelihood_blocks_find_dominant_ts([observable.likelihood],cfg.databg)
        clh = lh.CombinedLikelihood(cfg.databg)
        clh.add_likelihood(observable.likelihood)
        clh.calc_cls()
        CLs = clh.getCLs()

        # add it to the legend.
        if CLs is not None and CLs > 0:
            if observable.likelihood._index is not None and cfg.databg in observable.likelihood._index.keys():
                indextag=r"Bin {},{:3.0f}\%".format(observable.likelihood._index[cfg.databg],100.*CLs)
            else:
                indextag=r"Correlated {:3.0f}\%".format(CLs)
        else:
            indextag="No exclusion"

        sigback_databg.setAnnotation('Title','Data as BG: {}'.format(indextag))
        drawonly.append(sigback_databg.path())
        sigback_databg.setAnnotation('ErrorBars', '1')
        sigback_databg.setAnnotation('LineColor', 'red')
        anaobjects.append(sigback_databg)

    if observable.thyplot:
        # things we only do if there's a SM prediction.
        theory = observable.thyplot
        if nostack:
            # in this case we are just plotting signal, without stacking it on any background
            if smtest:
                sigback_smbg   = None
            else:
                sigback_smbg   = observable.sigplot
        else:
            sigback_smbg   = observable.stack_smbg
        
        # set annotations for the data-as-background signal plo
        theory.setAnnotation('RemoveOptions', '0')
        theory.setAnnotation('ErrorBars', '1')
        theory.setAnnotation('LineColor', 'green')
        theory.setAnnotation('ErrorBands','1')
        theory.setAnnotation('ErrorBandColor','green')
        theory.setAnnotation('ErrorBandOpacity','0.3')
        drawonly.append(theory.path())
        anaobjects.append(theory)

        # get the dominant test likelihood for this plot.
        contur.factories.likelihood.likelihood_blocks_find_dominant_ts([observable.likelihood],cfg.smbg)
        clh = lh.CombinedLikelihood(cfg.smbg)
        clh.add_likelihood(observable.likelihood)

        if smtest:

            # Calculate the compatibility between SM and data, using chi2 survival for the number of points
            pval = observable.get_sm_pval()

            # add the SM vs data compatibility to the legend.
            if observable.likelihood._index is not None and cfg.smbg in observable.likelihood._index.keys():
                indextag="p (Bin {})={:4.2f}".format(observable.likelihood._index[cfg.smbg],pval)
            else:
                indextag="p = {:4.2f}".format(pval)

            theory.setAnnotation('Title','{}, {}'.format(theory.title(),indextag))
            
        else:
          
            # dont do all this stuff if we are running without signal.
            # Calculate the smbg CLs for this individual plot 
            clh.calc_cls()
            CLs = clh.getCLs()

            contur.factories.likelihood.likelihood_blocks_find_dominant_ts([observable.likelihood],cfg.expected)
            clh = lh.CombinedLikelihood(cfg.expected)
            clh.add_likelihood(observable.likelihood)
            clh.calc_cls()
            CLs_exp = clh.getCLs()

            # add them to the legend.
            if CLs is not None and CLs_exp is not None and CLs > 0:
                if observable.likelihood._index is not None and cfg.smbg in observable.likelihood._index.keys():
                    indextag=r"Bin {},{:3.0f}\% ({:3.0f}\% expected)".format(observable.likelihood._index[cfg.smbg],100.*CLs,100.*CLs_exp)
                else:
                    indextag=r"Correlated {:3.0f}\% ({:3.0f}\% expected)".format(100.*CLs,100.*CLs_exp)
            else:
                indextag="No exclusion"

            # set annotations for the sm-as-background signal plot
            sigback_smbg.setAnnotation('Title','SM as BG: {}'.format(indextag))
            sigback_smbg.setAnnotation('ErrorBars', '1')
            sigback_smbg.setAnnotation('LineColor', 'blue')
            sigback_smbg.setPath(sigback_smbg.path()+"-SM")
            drawonly.append(sigback_smbg.path())
            anaobjects.append(sigback_smbg)


            
    # Now the overall attributes for the plot.
    plot = Plot()
    plot['RatioPlotYLabel'] = '(Bkgd+BSM)/Bkgd'
    
    # Get any updated attributes from plotinfo files
    plotdirs = ['/']
    plotparser = rivet.mkStdPlotParser(plotdirs, )
    for key, val in plotparser.getHeaders(refdata.path()).items():
        plot[key] = val

    plot['DrawOnly'] = ' '.join(drawonly).strip()
    plot['Legend'] = '1'
    plot['MainPlot'] = '1'
    #plot['LogY'] = '1'
    plot['RatioPlot'] = '1'
    plot['RatioPlotSameStyle'] = 0
    plot['RemoveOptions'] = '1'
    #plot['RatioPlotYMin'] = '0.0'
    # plot['RatioPlotMode'] = 'default'
    plot['RatioPlotMode'] = 'deviation'
    plot['RatioPlotYMin'] = '-3.0'
    plot['RatioPlotYMax'] = '3.0'
    if smtest:
        plot['RatioPlotYLabel'] = 'SM/Data'
    else:
        plot['RatioPlotYLabel'] = '(Bkgd+BSM)/Data'

    plot['RatioPlotErrorBandColor'] = 'Yellow'
    plot['RatioPlotReference'] = refdata.path()

    # now the output directory and file
    output = str(plot)
    from io import StringIO
    sio = StringIO()
    yoda.writeFLAT(anaobjects, sio)
    output += sio.getvalue()
    #ana, tag = splitPath(refdata.path())
    ana, tag = splitPath(observable.signal.path())
    outdir = os.path.join(cfg.plot_dir,observable.pool,ana)
    mkoutdir(outdir)
    outfile = '{}.dat'.format(tag)
    outfilepath = os.path.join(outdir, outfile)
    f = open(outfilepath, 'w')
    f.write(output)
    f.close()


def mkScatter2D(s1):
    """
    Make a Scatter2D from a Scatter1D by treating the points as y values and adding dummy x bins.
    """

    import yoda
    rtn = yoda.Scatter2D()

    xval = 0.5
    for a in s1.annotations():
        rtn.setAnnotation(a, s1.annotation(a))

    rtn.setAnnotation("Type", "Scatter2D");

    for point in s1.points():

        ex_m = xval-0.5
        ex_p = xval+0.5

        y = point.x()
        ey_p = point.xMax() - point.x()
        ey_m = point.x()    - point.xMin()

        pt = yoda.Point2D(xval, y, (0.5,0.5), (ey_p,ey_m))
        rtn.addPoint(pt)
        xval = xval + 1.0

    return rtn

def walklevel(some_dir, level=1):
    """
    Like os.walk but can specify a level to walk to
    useful for managing directories a bit better

    https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
    """
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


def newlogspace(start, stop, num=50, endpoint=True, base=10.0, dtype=None):
    """
    Numpy logspace returns base^start to base^stop, we modify this here so it returns logspaced between start and stop
    """
    return np.logspace(np.log(start)/np.log(base), np.log(stop)/np.log(base), num, endpoint, base, dtype)


def get_inspire(inspire_id):
    """
    Function to query InspireHEP database and return a dictionary containing the metadata

    extracted/adapted from rivet-findid

    :arg inspire_id: the ID of the publication on Inspire
    :type inspire_id: ``string``

    :return: pub_data ``dictionary`` -- selected metadata as a dictionary of bytes
    """

    try:
        from urllib.request import urlopen
    except ImportError:
        from urllib2 import urlopen
    import json
    url = "https://inspirehep.net/api/literature/{}".format(inspire_id)

    ## Get and test JSON
    try:
        cfg.contur_log.debug("Querying inspire: {}".format(inspire_id))
        response = urlopen(url)
        cfg.contur_log.debug("Success")
    except Exception as e:
        raise cfg.ConturError("Error opening URL {}: {}".format(url,e))

    metadata = json.loads(response.read().decode("utf-8"))
    if metadata.get("status", "") == 404:
        raise cfg.ConturError('ERROR: id {} not found in the InspireHEP database\n'.format(inspire_id))

    try:
        md=metadata["metadata"]
    except KeyError as ke:
        raise cfg.ConturError("Could not find metadata for inspire ID {}".format(inspire_id))

    pub_data = {}

    pub_data['bibkey']=md["texkeys"][0]
    biburl = metadata["links"]["bibtex"]

    cfg.contur_log.debug("Querying inspire: {}".format(biburl))
    pub_data['bibtex']=urlopen(biburl).read()
    cfg.contur_log.debug("Success")

    return pub_data

def permission_to_continue(message):
    """Get permission to continue program"""
    permission = ""
    while permission.lower() not in ['no', 'yes', 'n', 'y']:
        permission = str(input("{}\n [y/N]: ".format(message)))
        if len(permission)==0:
            permission = 'N'

    if permission.lower() in ['y', 'yes']:
        return True
    else:
        return False


class Plot(dict):
    ''' A tiny Plot object to help writing out the head in the .dat file '''

    def __repr__(self):
        return "# BEGIN PLOT\n" + "\n".join("%s=%s" % (k, v) for k, v in self.items()) + "\n# END PLOT\n\n"


def cleanupCommas(text):
    '''
    Replace commas and multiple spaces in text by single spaces
    '''
    text=text.replace(","," ")
    
    while "  " in text:
        text=text.replace("  ", " ")
    
    return text.strip()

def remove_brackets(text):
    '''
    remove any brackets from text, and try to turn it into a float.
    return None if not possible.
    '''
    res = text.split("(")[0]+text.split(")")[-1]
    try:
        res = float(res)
    except:
        res = None
        
    return res

def compress_particle_info(name,info):
    ''' 
    Turns a dictionary of properties of the particle <name> into simple string-formatted
    dictionary suitable for storing as parameters: 
    (removes commas, backslashes and spaces and puts <particlename>_<property>)

    '''

    info_dict = {}

    info_dict["{}_mass".format(name)]=info["mass"]
    info_dict["{}_width".format(name)]=info["width"]

    for decay, bf in info.items():
        if not (decay=="mass" or decay=="width"):
            decay = decay.replace(",","")
            decay = decay.replace(" ","")
            decay = decay.replace("\\","")
            info_dict["{}_{}".format(name,decay)]=bf

    return info_dict
    

def compress_xsec_info(info,matrix_elements):
    '''
    compresses a dict of subprocess cross sections into a format they can be stored as AUX params
    (removes commas, backslashes and spaces and puts AUX:<name>_<property>)
    Also, if matrix_elements is not None, then remove any which are not in it.

    '''

    info_dict = {}
    for name, value in info.items():
        name = name.replace(",","")
        name = name.replace(" ","")
        name = name.replace("\\rightarrow","_")
        name = name.replace("\\","")
        if matrix_elements is None or name in matrix_elements:
            name = "AUX:{}".format(name)
            info_dict[name]=value
        
    return info_dict

def hack_journal(bibtex):
    ''' 
    Add a dummy journal field if absent, to stop sphinx barfing.
    '''
    if b"journal" in bibtex:
        return bibtex
    else:
        newbibtex = bibtex[:-3]+b',journal = "no journal"\n}\n'
        return newbibtex

def find_ref_file(analysis,theory=False):
    '''
    return the REF data file name and path for analysis with name a_name
    if not found, return an empty string.
    '''
    import rivet
    if theory:
        yoda_name = analysis.name+"-Theory.yoda"
    else:
        yoda_name = analysis.shortname+".yoda"
    f = rivet.findAnalysisRefFile(yoda_name)
    if len(f)==0:
        yoda_name = yoda_name+".gz"
        f = rivet.findAnalysisRefFile(yoda_name)

    return f


    
