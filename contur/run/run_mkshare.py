# make the shared area of a common contur install

import os, glob
import subprocess

import rivet

import contur
import contur.config.config as cfg
from contur.run.arg_utils import setup_common
from contur.data.data_objects import Beam
import contur.data as cdb
import contur.data.data_access_db
import contur.data.static_db

import contur.util.utils as cutil


def write_sm_file(ana,out_dir,text_string):
    """
    Write an rst file describing the theory predictions available for this analysis.

    :param ana:  the analysis object
    :param out_dir: name of the directory the write to
    :param text_string: an rst-style link, with text, will be appended to this and returned.

    """    

    th_desc = ana.sm()
    ana_file_stem = ana.name.replace(":","_").replace("=","_")
    ana_file_out = os.path.join(out_dir,"SM",ana_file_stem+".rst")
    dat_file_dir = cfg.path("data","share",cfg.smdir,ana.poolid,ana.shortname)

    if th_desc:
        text_string += " SM theory predictions are available :doc:`here <SM/{}>`.\n".format(ana_file_stem)
        th_str = ":orphan:\n\nStandard Model Predictions for {}\n{}\n\n".format(ana_file_stem,"="*(31+len(ana_file_stem)))
        for prediction in th_desc:
            cfg.contur_log.debug("Getting info for theory prediction {} for {}".format(prediction.short_description,ana.shortname))
            insp_ids = prediction.inspids.split(',')
            bibkeys = ""
            for insp_id in insp_ids:
                try:
                    paper_data = cutil.get_inspire(insp_id) 
                    bibkeys+=str(paper_data['bibkey'])
                    bibkeys+=','
                except cfg.ConturError as e:
                    cfg.contur_log.warning("Could not find bibtex key for inspire ID {} in {}: {}".format(insp_id,ana.name,e))

            if len(bibkeys)>0:
                th_str += "\n {} :cite:`{}`: {} \n\nStored in file: {} \n".format(prediction.short_description,bibkeys[:-1],prediction.long_description,prediction.file_name)
            else:
                th_str += "\n {} (Could not find bibtex key) {} \n\nStored in file: {} \n".format(prediction.short_description,prediction.long_description,prediction.file_name)

        th_str += "\n"

        # now make the figures
        ana_file_dir = os.path.join(out_dir,"SM",ana_file_stem)
        cutil.mkoutdir(ana_file_dir)
        mp_command = ["make-plots"]
        mp_command.append("-f")
        mp_command.append("png")
        mp_command.append("-o")
        mp_command.append(ana_file_dir)
        cfg.contur_log.info("Looking for SM dat files in {}".format(dat_file_dir))
        #print("Looking for SM dat files in {}".format(dat_file_dir))
        got_dats=False
        for dat_file in glob.glob(os.path.join(dat_file_dir,"*.dat")):
            got_dats=True
            mp_command.append(dat_file)
            th_str += ".. figure:: "+ana_file_stem+"/"+os.path.basename(os.path.splitext(dat_file)[0])+".png"+"\n           :scale: 80%\n\n"

        if got_dats: 
            subprocess.Popen(mp_command).wait()
            ana_file = open(ana_file_out, 'w')
            ana_file.write(th_str)
        else:   
            print("No dat files found for {}".format(dat_file_dir))
            
    else:
        text_string += ":red:`No SM theory predictions available for this analysis.` \n"

    return text_string

def generate_rivet_anas(webpages):
    """
    Generate various rivet analysis listings from the Contur database.

    - the .ana files for Herwig running

    - the script to set the analysis list environment variables

    - (optionally) the contur webpage listings.

    :param webpages: if true, also write out the contur webpage listings.
    
    """
    # statics
    rivettxt = "insert Rivet:Analyses 0 "
    
    # make the directory if it doesn't already exist
    output_directory = cfg.output_dir
    cutil.mkoutdir(output_directory)
    cutil.mkoutdir(output_directory+"/SM")

    # open file for the environment setters
    fl = open(output_directory + "/analysis-list", 'w')

    known_beams = cdb.static_db.get_beams()
    envStrings = {}

    # build the .ana and env variable setters
    for beam in known_beams:
        analysis_list = cdb.static_db.get_analyses(beam=beam,filter=False) 
        f = open(os.path.join(output_directory, beam.id + ".ana"), 'w')
        envStrings[beam.id] = "export CONTUR_RA{}=\"".format(beam.id)
        for analysis in analysis_list:
            f.write(rivettxt + analysis.name + " # " + analysis.summary() + "\n")
            envStrings[beam.id] = envStrings.get(beam.id) + analysis.name + ","

        f.close()

    for estr in envStrings.values():
        estr = estr[:len(estr) - 1] + "\""
        fl.write(estr + "\n \n")

    fl.close()
        
    cfg.contur_log.info("Analysis and environment files written to {}".format(output_directory))

    if not webpages:
        return

    web_dir = os.getenv('CONTUR_WEBDIR')
    if web_dir == None:
        web_dir = output_directory
    else:
        web_dir = os.path.join(web_dir,"datasets")

    cfg.contur_log.info("Writing graphics output to {}".format(web_dir))

        
    # style stuff
    style_stuff = ".. raw:: html \n \n <style> .red {color:red} </style> \n \n.. role:: red\n\n"

    # open file for the web page list
    data_list_file = open(os.path.join(web_dir,"data-list.rst"), 'w')
    data_list = "Current Data \n------------ \n"

    bvetoissue = "\nb-jet veto issue\n---------------- \n\n *The following measurements apply a detector-level b-jet veto which is not part of the particle-level fiducial definition and therefore not applied in Rivet. Also applies to CMS Higgs-to-WW analysis. Off by default, can be turned on via command-line, but use with care.* \n"

    higgsww = "\nHiggs to WW\n----------- \n\n *Typically involve large data-driven top background subtraction. If your model contributes to the background as well the results maybe unreliable. Off by default, can be turned on via command-line.* \n"

    higgsgg = "\nHiggs to diphotons\n------------------ \n\n *Higgs to two photons use a data-driven background subtraction. If your model predicts non-resonant photon production this may lead to unreliable results. On by default, can be turned off via command-line.* \n"

    ratios = "\nRatio measurements\n------------------ \n\n *These typically use SM theory for the denominator, and may give unreliable results if your model contributes to both numerator and denominator. On by default, can be turned off via command-line.* \n"

    searches = "\nSearches\n-------- \n\n *Detector-level, using Rivet smearing functions. Off by default, can be turned on via command-line.*\n"

    nutrue = "\nNeutrino Truth\n-------------- \n\n *Uses neutrino flavour truth info, may be misleading for BSM. Off by default, can be turned on via command-line.*\n"

    for ana in sorted(cdb.static_db.get_analyses(filter=False), key=lambda ana: ana.poolid):

        pool = ana.get_pool()
        pool_str = "\n Pool: **{}**  *{}* \n\n".format(pool.id,pool.description)
        
        tmp_str = "   * `{} <https://rivet.hepforge.org/analyses/{}.html>`_, ".format(ana.name,ana.shortname)
        tmp_str += "{} :cite:`{}`. ".format(ana.summary(),ana.bibkey())

        tmp_str = write_sm_file(ana,web_dir,tmp_str)

        if cdb.static_db.hasRatio(ana.name):
            if pool.id in ratios:
                ratios += tmp_str
            else:
                ratios += pool_str + tmp_str

        elif cdb.static_db.hasSearches(ana.name):
            if pool.id in searches:
                searches += tmp_str
            else:
                searches += pool_str + tmp_str

        elif cdb.static_db.hasHiggsgg(ana.name):
            if pool.id in higgsgg:
                higgsgg += tmp_str
            else:
                higgsgg += pool_str + tmp_str

        elif cdb.static_db.hasHiggsWW(ana.name):
            if pool.id in higgsww:
                higgsww += tmp_str
            else:
                higgsww += pool_str + tmp_str

        elif cdb.static_db.hasNuTrue(ana.name):
            if pool.id in nutrue:
                nutrue += tmp_str
            else:
                nutrue += pool_str + tmp_str

        else:
            if pool.id in data_list:
                data_list += tmp_str
            else:
                data_list += pool_str + tmp_str

        if cdb.static_db.hasBVeto(ana.name):
            if pool.id in bvetoissue:
                bvetoissue += tmp_str
            else:
                bvetoissue += pool_str + tmp_str


    data_list_file.write(style_stuff)
    data_list_file.write(data_list)
    data_list_file.write(ratios)
    data_list_file.write(higgsgg)
    data_list_file.write(searches)
    data_list_file.write(nutrue)
    data_list_file.write(higgsww)
    data_list_file.write(bvetoissue)

    cfg.contur_log.info("Data list written to {}".format(web_dir))
    


def main(args):
    """
    Main programme to run over the known analysis and build SM theory yodas from the TheoryRaw or REF areas.
    """
    cfg.setup_logger(filename="contur_mkshare.log")
    setup_common(args)
    print("Writing log to {}".format(cfg.logfile_name))

    cfg.results_dbfile = os.path.join(cfg.output_dir,cfg.results_dbfile)
    cfg.models_dbfile = os.path.join(cfg.output_dir,cfg.models_dbfile)

    cfg.contur_log.info("Making shared area in {}".format(cfg.output_dir))
                
    generate_rivet_anas(args['WEBPAGES'])
    cdb.data_access_db.generate_model_and_parameter(model_db=True)
