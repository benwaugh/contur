#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.6
HERWIG_VERSION=7.2.3
PYTHON_VERSION=py3

CONTUR_BRANCHES="contur-2.3.0"
if [[ "$MAIN" = 1 ]]; then CONTUR_BRANCHES="main $CONTUR_BRANCHES"; fi

# IMPORTANT: put 'latest' version last in the list: it gets reused after the loop
for CONTUR_BRANCH in $CONTUR_BRANCHES; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}

    MSG="Building contur-herwig with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, Herwig=$HERWIG_VERSION, Python=$PYTHON_VERSION"
    tag="hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION-$PYTHON_VERSION"

    if [[ "$CONTUR_BRANCH" = "master" && "$FORCE" = 1 ]]; then BUILDFLAGS="--no-cache"; fi

    docker build . -f Dockerfile $BUILDFLAGS \
           --build-arg RIVET_VERSION=$RIVET_VERSION \
           --build-arg HERWIG_VERSION=$HERWIG_VERSION \
           --build-arg PYTHON_VERSION=$PYTHON_VERSION \
           --build-arg CONTUR_BRANCH=$CONTUR_BRANCH \
           -t $tag
    docker tag $tag hepstore/contur-herwig:$CONTUR_VERSION-$PYTHON_VERSION

    if [[ "$PUSH" = 1 ]]; then
        docker push $tag && sleep 30s
        docker push hepstore/contur-herwig:$CONTUR_VERSION-$PYTHON_VERSION && sleep 30s
    fi

    docker tag hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION-py3 hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION
    docker tag hepstore/contur-herwig:$CONTUR_VERSION-py3 hepstore/contur-herwig:$CONTUR_VERSION
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION && sleep 30s
        docker push hepstore/contur-herwig:$CONTUR_VERSION && sleep 30s
    fi

done

if [[ "$LATEST" = 1 ]]; then
    docker tag hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION-py3 hepstore/contur-herwig:latest
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur-herwig:latest
    fi
fi
