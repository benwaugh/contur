#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.6
PYTHIA_VERSION=8307

CONTUR_BRANCHES="contur-2.3.0"
if [[ "$MAIN" = 1 ]]; then CONTUR_BRANCHES="main $CONTUR_BRANCHES"; fi

# IMPORTANT: put 'latest' version last in the list: it gets reused after the loop
for CONTUR_BRANCH in $CONTUR_BRANCHES; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}
    for PYTHON_VERSION in py3; do

        MSG="Building contur-pythia with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, Pythia=$PYTHIA_VERSION, Python=$PYTHON_VERSION"
        tag="hepstore/contur-pythia:$CONTUR_VERSION-$PYTHIA_VERSION-$PYTHON_VERSION"

        if [[ "$CONTUR_BRANCH" = "master" && "$FORCE" = 1 ]]; then BUILDFLAGS="--no-cache"; fi

        docker build . -f Dockerfile $BUILDFLAGS \
               --build-arg RIVET_VERSION=$RIVET_VERSION \
               --build-arg PYTHIA_VERSION=$PYTHIA_VERSION \
               --build-arg CONTUR_BRANCH=$CONTUR_BRANCH \
               -t $tag
        docker tag $tag hepstore/contur-pythia:$CONTUR_VERSION-$PYTHON_VERSION

        if [[ "$PUSH" = 1 ]]; then
            docker push $tag && sleep 30s
            docker push hepstore/contur-pythia:$CONTUR_VERSION-$PYTHON_VERSION && sleep 30s
        fi
    done

    docker tag hepstore/contur-pythia:$CONTUR_VERSION-$PYTHIA_VERSION-py3 hepstore/contur-pythia:$CONTUR_VERSION-$PYTHIA_VERSION
    docker tag hepstore/contur-pythia:$CONTUR_VERSION-py3 hepstore/contur-pythia:$CONTUR_VERSION
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur-pythia:$CONTUR_VERSION-$PYTHIA_VERSION && sleep 30s
        docker push hepstore/contur-pythia:$CONTUR_VERSION && sleep 30s
    fi

done

if [[ "$LATEST" = 1 ]]; then
    docker tag hepstore/contur-pythia:$CONTUR_VERSION-$PYTHIA_VERSION-py3 hepstore/contur-pythia:latest
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur-pythia:latest
    fi
fi
