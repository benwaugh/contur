contur package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   contur.config
   contur.data
   contur.factories
   contur.plot
   contur.run
   contur.scan
   contur.util


Module contents
---------------

.. automodule:: contur
   :members:
   :undoc-members:
   :show-inheritance:


Executables
-----------

`contur`
~~~~~~~~
Main executable for running contur analysis on a grid of yoda files, a single yoda file, or a yoda stream

.. argparse::
   :ref: contur.run.run_analysis.doc_argparser
   :prog: contur

`contur-batch`
~~~~~~~~~~~~~~
Run a series of batch jobs to either 1) build a grid of yoda files or 2) analyse a grid of yoda files

.. argparse::
   :ref: contur.run.run_batch_submit.doc_argparser
   :prog: contur-batch

`contur-export`
~~~~~~~~~~~~~~~
Export a map file as a csv file.

`contur-extract-xs-bf`
~~~~~~~~~~~~~~~~~~~~~~
Extract the cross sections and branching ratios from a single event generator log


`contur-gridtool`
~~~~~~~~~~~~~~~~~
Collection of utilities for operating on grids of yoda files

.. argparse::
   :ref: contur.run.run_grid_tools.doc_argparser
   :prog: contur-gridtool


`contur-mapmerge`
~~~~~~~~~~~~~~~~~
Merge a list of map files


`contur-mkbib`
~~~~~~~~~~~~~~
Make the contur webpage bibliography


`contur-mkhtml`
~~~~~~~~~~~~~~~
Make an html summary, with links to plots, for a single yoda file on which contur has been run


`contur-mkshare`
~~~~~~~~~~~~~~~~
Make the contents of the data/share area


`contur-mkthy`
~~~~~~~~~~~~~~
Make theory reference yodas (/THY/) from raw theory archives.

`contur-oracle`
~~~~~~~~~~~~~~~
Run the oracle

	  
`contur-plot`
~~~~~~~~~~~~~
Main executuble for making heatmap and contour plots from a map file

.. argparse::
   :ref: contur.run.run_plot.doc_argparser
   :prog: contur-plot

`contur-scan-xs-bf`
~~~~~~~~~~~~~~~~~~~
Extract the cross sections and branching ratios from generator log files in a grid

`contur-smtest`
~~~~~~~~~~~~~~~
Run contur analysis on data vs SM theory

.. argparse::
   :ref: contur.run.run_smtest.doc_argparser
   :prog: contur-smtest

`contur-zoom`
~~~~~~~~~~~~~
zoom in on a parameter region (probably obsolete/broken)

