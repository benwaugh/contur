#! /usr/bin/env python

try:
    from setuptools import setup,find_packages
except ImportError:
    from distutils.core import setup

import os
os.environ["CONTUR_ROOT"] = os.path.abspath(".")
import contur
__doc__ = contur.__doc__

setup(name = "contur",
      version = contur.__version__,
      #packages = ["contur"],
      packages=find_packages(),
      package_dir = {"contur" : "contur"},
      package_data = {"contur" : ["data/DB/analyses.db", "data/Rivet/*.so", "data/Theory/*.yoda"]},
      scripts = [os.path.join("bin", i) for i in os.listdir("bin") if i.startswith("contur")],
      install_requires = ["numpy", "scipy", "pandas", "configobj", "matplotlib",
                          "tqdm", "pytest", "pyyaml", "pyslha"],
      author = "The Contur Collaboration",
      #author_email = 'andy.buckley@cern.ch',
      description = 'Model interpretation of collider-physics measurements',
      long_description = __doc__,
      keywords = 'BSM UFO SLHA LHC HEP physics particle',
      license = 'GPL')
