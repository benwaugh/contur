# -*- bash -*-
mydir="$( dirname "${BASH_SOURCE[0]}" )"
export CONTUR_ROOT="$( cd "$mydir" 2>&1 > /dev/null && echo "$PWD" )"

export RIVET_DATA_PATH=$(echo $CONTUR_ROOT/data/Rivet:$CONTUR_ROOT/data/Theory:$RIVET_DATA_PATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export RIVET_ANALYSIS_PATH=$(echo $CONTUR_ROOT/data/Rivet:$RIVET_ANALYSIS_PATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export PYTHONPATH=$(echo $CONTUR_ROOT:$PYTHONPATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export PATH=$(echo $CONTUR_ROOT/bin:$PATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export PYTHONPATH=$PYTHONPATH:$PWD
# This file won't exist until make has been run
# TODO: we need a better way to do this
ALIST=$CONTUR_ROOT/data/share/analysis-list
test -f $ALIST && source $ALIST

# TODO: integrate the visualiser properly, via bin/contur-visualize
chmod +x $CONTUR_ROOT/contur-visualiser/contur-visualiser

# Run some checks and reassure the user with messages if all looks good
$CONTUR_ROOT/bin/check-contur-deps #-V
